package com.tul.involved;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InvolvedBeApplication {

    public static void main(String[] args) {
        SpringApplication.run(InvolvedBeApplication.class, args);
    }

}
